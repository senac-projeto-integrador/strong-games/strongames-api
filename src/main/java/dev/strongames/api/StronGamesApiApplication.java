package dev.strongames.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StronGamesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StronGamesApiApplication.class, args);
    }

}
